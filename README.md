**Todoodles : A basic Opensource Todo manager written in Python**

This Todo manager will be managing tasks based on sessions. The sessions can be named, thereby allowing the user to create custom session files. 
The session files are stored in the "tfiles" directory in the same directory where the rest of the code.

---

**Technical details**

In order to run the program, the following modules need to be present.

* WxPython (>= 3.0.2.0)

---

**Module structure**

The modules are structured as follows:

```bash
./
├── Helper
│   ├── FileManager.py
│   ├── FrameHelper.py
│   ├── __init__.py
│   ├── ListCtrlHelper.py
├── Logger
│   ├── __init__.py
│   ├── LogHelper.py
├── logs
│   └── todo_py.log
├── main.py
├── README.md
├── tfiles
│   └── programming_sb.tfile
└── WxApp
    ├── __init__.py
    ├── WxApp.py
```

---
