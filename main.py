#!/usr/bin/env python

from wx import App
from WxApp import WxApp
from wx import DEFAULT_FRAME_STYLE
from Logger import LogHelper
from logging import DEBUG
from os import path, getcwd, mkdir

def create_requisites():
    # First check if the directories exist or not
    if not path.isdir(getcwd() + "/tfiles/"):
        # Create the directories
        mkdir(getcwd() + "/tfiles/")
    if not path.isdir(getcwd() + "/logs/"):
        mkdir(getcwd() + "/logs/")

def main():
    # Create the required directories before the Application is started
    create_requisites()
    logger = LogHelper.LogHelper(name="TODO_PY", log_level=DEBUG, 
        log_file = getcwd() + "/logs/todo_py.log")
    app = App(redirect=False)
    logger.info("Wx Application Init")
    wapp = WxApp.WxFrame(logger=logger, parent=None, id_custom=100,
                         title="TODO Lister",
                         style=DEFAULT_FRAME_STYLE, )
    wapp.frame_show(show=True)
    app.MainLoop()


if __name__ == '__main__':
    main()
