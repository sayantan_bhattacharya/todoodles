#!/usr/bin/env python

# Author : sb

from wx import Frame, MenuBar, Menu, MessageBox, Panel, BoxSizer, ComboBox
from wx import ArtProvider, BitmapButton, StaticText, ListCtrl, FileDialog
from wx.lib.pubsub import setuparg1, pub as Publisher
from wx import ALIGN_CENTER_VERTICAL, ALL, ALIGN_CENTER, LC_REPORT, EXPAND
from wx import ID_ADD, ID_ABOUT, ID_ANY, ID_OK
from wx import EVT_BUTTON, EVT_MENU, EVT_COMBOBOX, FD_SAVE 
from wx import EVT_COMBOBOX_DROPDOWN
from wx import DEFAULT_FRAME_STYLE
from wx import ART_PLUS, ART_NEW
from wx import VERTICAL, HORIZONTAL
from fileinput import FileInput
from Helper import FrameHelper, ListCtrlHelper, FileManager
from os import getcwd


class WxFrame(Frame):
    # Member variables
    init = None
    todo_save_file = None  # list file save location

    # Menu bar variables
    menu_bar = None
    help_menu = None
    h_about = None

    # Panel and Sizer variables
    panel = None
    main_sizer = None
    text_sizer = None
    button_sizer = None
    list_sizer = None
    combo_sizer = None

    # Text variables
    banner_text = None

    # Button variables
    add_btn = None
    new_sess_btn = None

    # Drop down menu items
    drop_down_files = None  # List of files
    file_combo = None

    # Details addition frame
    details_frame = None

    # Create a list control for displaying the list
    task_list = None
    task_index = None

    # Constants
    COL_VALUE = 1

    # File Manager
    fmgr = None

    def __init__(self, logger, parent=None, id_custom=ID_ANY,
                 title="Default Frame", style=None):
        if style is None:
            style = DEFAULT_FRAME_STYLE
        super(WxFrame, self).__init__(parent, id_custom, title, style=style)
        self.logger = logger
        self.init = True
        self.fmgr = FileManager.FMgr(self.logger)
        self.logger.debug("Wx Frame Init complete")
        self.logger.debug("Setting up Publisher")
        Publisher.subscribe(self._on_pubsub, ("show.mainframe"))
        Publisher.subscribe(self._on_check_box, ("task_list.mainframe"))
        self.logger.debug("Setting up task_index")
        self.task_index = 0

    def frame_show(self, show=True):
        # Read the configuration file first
        if self.init:
            # Set the frame details now
            self.logger.debug("Setting up the widgets")
            self.set_widgets()

            # Set the panel now
            self.logger.debug("Setting up the layout")
            self.set_layout()

            # Show the frame finally
            self.logger.debug("Centering the frame on the screen")
            self.CenterOnScreen(True)
            self.logger.info("Showing the frame")
            self.Show(show)

    def set_layout(self):
        self.logger.debug("Creating Wx Panel instance")
        self.panel = Panel(self)

        # Set up the sizer
        self.logger.debug("Setting up the Main Sizer")
        self.main_sizer = BoxSizer(VERTICAL)
        self.logger.debug("Setting up rest of the sizers")
        self.text_sizer = BoxSizer(VERTICAL)
        self.button_sizer = BoxSizer(HORIZONTAL)  # Make it horizontal
        self.list_sizer = BoxSizer(VERTICAL)
        self.combo_sizer = BoxSizer(VERTICAL)

        # Add to the text sizer
        self.logger.debug("Setting up the banner text")
        self.banner_text = StaticText(self.panel, label="TODO List",
                                      style=ALIGN_CENTER_VERTICAL)
        self.logger.debug("Adding the banner_text to text_sizer")
        self.text_sizer.Add(self.banner_text, 0, ALL | ALIGN_CENTER, 5)

        # Add the buttons now - trying a change to BitmapButton
        self.add_btn = BitmapButton(self.panel, ID_ADD, 
            bitmap = ArtProvider.GetBitmap(ART_PLUS),
                              style=ALIGN_CENTER_VERTICAL)
        self.new_sess_btn = BitmapButton(self.panel, ID_ANY, 
            bitmap = ArtProvider.GetBitmap(ART_NEW), style = ALIGN_CENTER)

        # Add the combo box/drop down now
        self.logger.info("Setting up the list of sessions")
        self.drop_down_files = self.fmgr.list_files()
        self.file_combo = ComboBox(self.panel, choices = self.drop_down_files)

        # Add the list control now
        self.logger.debug("Setting up the task_list")
        self.task_list = ListCtrlHelper.ChkListCtrl(self.panel, -1, 
            style = LC_REPORT)
        self.logger.debug("Inserting the columns")
        self.task_list.InsertColumn(0, "Priority")
        self.task_list.InsertColumn(1, "Task Name")
        self.task_list.Arrange()

        # Bind the specific function to the button
        self.add_btn.Bind(EVT_BUTTON, self._on_add)
        self.new_sess_btn.Bind(EVT_BUTTON, self._on_new)

        # Add the add button to the specific button sizer
        self.button_sizer.Add(self.add_btn, 0, ALL | ALIGN_CENTER, 5)
        self.button_sizer.Add(self.new_sess_btn, 0, ALL | ALIGN_CENTER, 5)

        # Bind event to combo box now
        self.file_combo.Bind(EVT_COMBOBOX, self._on_combo)
        self.file_combo.Bind(EVT_COMBOBOX_DROPDOWN, self._on_drop_down)

        # Adding the custom list control to its own sizer
        self.list_sizer.Add(self.task_list, proportion = 1, 
            flag = EXPAND | ALL, border = 5)

        # Adding the combo box to the respective sizer
        self.combo_sizer.Add(self.file_combo, 0, ALL | ALIGN_CENTER | EXPAND, 5)


        # Adding banner text to the main sizer
        self.main_sizer.Add(self.text_sizer, 0, ALL | ALIGN_CENTER, 5)
        self.main_sizer.Add(self.button_sizer, 0, ALL | ALIGN_CENTER, 5)
        self.main_sizer.Add(self.combo_sizer, 0, ALL | ALIGN_CENTER | EXPAND, 5)
        self.main_sizer.Add(self.list_sizer, 1, ALL | EXPAND, 5)

        # Add the main sizer to the panel
        self.panel.SetSizerAndFit(self.main_sizer)

    def _on_drop_down(self, event):
        self.logger.debug("Drop down event ID : " + str(event.GetId()))

    def _on_new(self, event):
        self.logger.debug("New Session event ID : " + str(event.GetId()))
        # create and show the save file dialog
        dlg = FileDialog(self, message = "Create Session file", 
            defaultDir = getcwd() + "/tfiles/", defaultFile = "", 
            wildcard = "*.tfile", style = FD_SAVE)
        filepath = None
        if dlg.ShowModal() == ID_OK:
            filepath = dlg.GetPath()
            self.logger.debug("Path of session file : " + filepath)
        dlg.Destroy()
        # Create the empty file now
        if filepath is not None:
            file = open(filepath, 'w')
            file.close()
        # Reload the list of files
        self.drop_down_files = self.fmgr.list_files()
        # clear the list first
        self.file_combo.Clear()
        for i in range(len(self.drop_down_files)):
            self.file_combo.Append(self.drop_down_files[i])
        if filepath in self.drop_down_files:
            self.file_combo.SetSelection(self.drop_down_files.index
                (filepath))
        self.todo_save_file = filepath

    def set_widgets(self):
        if self.init:
            # Set up the menus first
            self.logger.debug("Creating instance of the menu bar and "
                              "the help menu")
            self.menu_bar = MenuBar()
            self.help_menu = Menu()

            self.h_about = self.help_menu.Append(ID_ABOUT, 
                "About Application", "TODO Lister application")

            self.logger.debug("Appending help menu item to menu bar")
            self.menu_bar.Append(self.help_menu, "&Help")

            # Binding functions
            self.logger.debug("Binding function for menu event")
            self.Bind(EVT_MENU, self._on_about, self.h_about)

            # Set the Menu Bar now
            self.SetMenuBar(self.menu_bar)

    def _on_about(self, event):
        self.logger.debug("About Event ID : " + str(event.GetId()))
        MessageBox("Author : Sayantan Bhattacharya(SB)\
            \r\nTODO Lister Program"
                   " for personal use of the author")

    def __populate_task_list(self):
        # Always save the todo_save_file in the function calling this function
        self.logger.debug("Setting the filename : " + self.todo_save_file)
        self.fmgr.set_filename(self.todo_save_file)
        self.logger.debug("Getting the tasks in a list")
        tasks = self.fmgr.read_file()

        self.logger.debug("Parsing the tasks and populating the list")
        for i in range(0, len(tasks)):
            if len(tasks) > 0:
                # Split the tasks and store the same in a list
                temp = tasks[i].split(':')
                self.task_list.Append([str(self.task_index + 1), 
                    str(temp[0].strip())])
                self.task_index += 1
        for i in range(0, len(tasks)):
            if len(tasks) > 0:
                temp = tasks[i].split(':')
                # Check out the items separately
                if str(temp[1].strip()) == 'True':
                    self.task_list.CheckItem(i)

    def _on_combo(self, event):
        self.logger.debug("Combo Box Event ID : " + str(event.GetId()))
        self.logger.info("Opening Session : " + str(self.file_combo.GetValue()))
        # Check if the filenames are same or not
        # If they are not, clear the list and then set the todo_save_file value
        if self.todo_save_file != self.file_combo.GetValue():
            self.task_list.DeleteAllItems()
            # reset the index to 0
            self.task_index = 0
            self.todo_save_file = self.file_combo.GetValue()

        self.logger.debug("Calling internal __populate_task_list function")
        self.fmgr.set_filename(self.todo_save_file)
        self.__populate_task_list()

    def _on_add(self, event):
        self.logger.debug("Add Event ID : " + str(event.GetId()))
        
        # On clicking add button, open a new frame and get the details
        self.details_frame = FrameHelper.FrameHelper(self.logger)
        self.details_frame.show_frame()

    def _on_pubsub(self, msg):
        self.logger.debug("Adding Task : " + str(msg.data))

        # Add the inserted item on the list control
        self.task_list.Append([str(self.task_index + 1), str(msg.data)])
        # Update the task index
        self.task_index += 1
        # Also write on the file
        self.__write_task_list()

    def __write_task_list(self):
        # first read the tasks already listed
        if self.todo_save_file is not None:
            self.fmgr.set_filename(self.todo_save_file)
            self.logger.debug("Reading the already documented tasks "
             "into a list")
            tasks = self.fmgr.read_file()
            # now get the count of items from the UI
            self.logger.debug("Getting the count of tasks listed in UI")
            count = self.task_list.GetItemCount()
            # Comparing the list of tasks
            if len(tasks) < count:
                self.logger.debug("Some task(s) need to be appended")
            elif len(tasks) == count:
                self.logger.debug("Some specific task(s) need to\
                 be updated")
            # For both the cases just re-write the file
            write_list = []
            for i in range(count):
                write_list.append(self.task_list.GetItem(itemId = i, 
                        col = self.COL_VALUE).GetText() + ' : ' + 
                str(self.task_list.IsChecked(i)))
            self.logger.debug("Sending out the list of tasks"
             "to the file manager function")
            # The filename has already been set - just send the write list to 
            # the function
            self.fmgr.update_file(write_list)

    def _on_check_box(self, tuple_in):
        # Log the tuple values
        self.logger.debug("Value received from task_list.mainframe : " + 
            str(tuple_in.data))

        # On checking a box, the file will be written with the appropriate data
        # Send in the values to the file manager
        # Write a function to perform the writing of the file properly
        self.__write_task_list()
