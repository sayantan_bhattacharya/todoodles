#!/usr/bin/env python

# Author : sb

from logging import getLogger, Formatter, FileHandler
from logging import DEBUG
from os import getcwd


class LogHelper:
    # Member variables
    logger = None
    log_level = None
    name = None
    handler = None
    log_formatter = None
    log_file = None
    init = None
    log_location = None

    def __init__(self, name=None, log_level=None, log_file=None):
        if log_level is None:
            self.log_level = DEBUG
        else:
            self.log_level = log_level
        if name is None:
            self.init = False
            return
        else:
            self.name = name
        if log_file is None:
            self.log_file = getcwd() + "/tpy_log_file"
        else:
            self.log_file = log_file
        self.set_logger()
        self.init = True

    def set_logger(self):
        self.logger = getLogger(self.name)
        self.logger.setLevel(self.log_level)

        # Set up the handler
        self.handler = FileHandler(self.log_file)
        self.handler.setLevel(self.log_level)

        # Format for logging
        self.log_formatter = Formatter('%(asctime)s :: %(name)s : '
                                       '%(levelname)s '
                                       '%(message)s')

        # Set the formatter
        self.handler.setFormatter(self.log_formatter)

        # Add handler to the logger
        self.logger.addHandler(self.handler)

    def debug(self, message=None):
        if self.init:
            if message is not None:
                self.logger.debug(message)

    def info(self, message=None):
        if self.init:
            if message is not None:
                self.logger.info(message)

    def error(self, message=None):
        if self.init:
            if message is not None:
                self.logger.error(message)

    def warning(self, message=None):
        if self.init:
            if message is not None:
                self.logger.warning(message)
