#!/usr/bin/env python

# Aim : Create another frame which will be taking in the information from the 
# user on the details of the TODO item
# Check if message dialog can do the work other than using the Frame class

from wx import Frame, Panel, StaticText, TextCtrl, Button, BoxSizer
from wx.lib.pubsub import setuparg1, pub as Publisher
from wx import ID_ANY, EVT_BUTTON
from wx import DEFAULT_FRAME_STYLE, VERTICAL
from wx import ALL, CENTER, EXPAND
from wx import MINIMIZE_BOX, MAXIMIZE_BOX, RESIZE_BORDER, RESIZE_BOX

class FrameHelper(Frame):
	# Logger
	logger = None

	# Panel and Sizers
	panel = None
	main_sizer = None

	# Temporary elements
	instr_text = None  # instruction text - StaticText
	msg_text = None  # Message Text - Text Ctrl
	close_btn = None

	# Text to be sent to the calling frame
	msg = None

	# Size constants
	WIDTH = 200
	HEIGHT = 100

	def __init__(self, logger, parent=None, id_custom=ID_ANY, 
		title="Enter details", style=None):
		# check if the style is None, if it is - change it to the default frame
		# style
		if style is None:
			style = DEFAULT_FRAME_STYLE
		super(FrameHelper, self).__init__(parent=parent, id=id_custom, 
			title=title, style=style & 
			~(MINIMIZE_BOX | MAXIMIZE_BOX | RESIZE_BORDER | RESIZE_BOX), 
			size = (self.WIDTH, self.HEIGHT))
		# Check the type of object logger variable has received
		if logger is not None:
			self.logger = logger

	def show_frame(self):
		self.logger.info("Showing the detail accepting frame on the screen")

		# Set up the layout
		self.set_layout()

		# Show the frame on the center of the screen
		self.CenterOnScreen(True)
		self.Show(True)

	def set_layout(self):
		self.logger.debug("Setting up the layout of the second frame")
		self.panel = Panel(self)

		# Setting up a message to be sent to the calling frame
		self.logger.debug("Setting up the StaticText and TextCtrl now")
		self.instr_text = StaticText(self.panel, 
			label = "Enter message to be sent to main frame")
		self.msg_text = TextCtrl(self.panel, value = "")
		self.logger.debug("Setting up the send button")
		self.close_btn = Button(self.panel, label = "Send")

		# Binding function for the button
		self.close_btn.Bind(EVT_BUTTON, self._on_send)

		# Setting up the sizers
		self.logger.debug("Setting up the sizers")
		self.main_sizer = BoxSizer(VERTICAL)

		# Temp variable having the align flags
		flags = ALL | CENTER

		# Adding the components to the sizer
		self.logger.debug("Adding the components to the sizer")
		self.main_sizer.Add(self.instr_text, 0, flags, 5)
		self.main_sizer.Add(self.msg_text, 0, flags | EXPAND, 5)
		self.main_sizer.Add(self.close_btn, 0, flags, 5)

		# Setting the sizer to the panel
		self.panel.SetSizer(self.main_sizer)

	def _on_send(self, event):
		self.logger.debug("Send event ID : " + str(event.GetId()))
		self.msg = self.msg_text.GetValue()

		# Send the message using Publisher
		Publisher.sendMessage(("show.mainframe"), self.msg)

		# Close the frame
		self.Close()
