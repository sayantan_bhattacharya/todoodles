#!/usr/bin/env python

from wx import ListCtrl
from wx.lib.mixins import listctrl as listmix
from wx.lib.pubsub import setuparg1, pub as Publisher

# TODO : Set the flag based on the value transferred to the class member 
# function

class ChkListCtrl(ListCtrl, listmix.CheckListCtrlMixin, 
	listmix.ListCtrlAutoWidthMixin):
	# Return Tuple
	tuple_return = None

	def __init__(self, *args, **kwargs):
		ListCtrl.__init__(self, *args, **kwargs)
		listmix.CheckListCtrlMixin.__init__(self)
		listmix.ListCtrlAutoWidthMixin.__init__(self)
		self.setResizeColumn(2)

	def OnCheckItem(self, index, flag):
		self.tuple_return = (index, flag)

		# Send out the message
		Publisher.sendMessage("task_list.mainframe", self.tuple_return)
