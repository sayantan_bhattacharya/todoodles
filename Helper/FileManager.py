#!/usr/bin/env python

# Author : sb

from pathlib2 import Path
from glob import glob
from fileinput import FileInput
from os import getcwd

class FMgr:
	# Logger details
	logger = None

	# File location and name
	filename = None

	def __init__(self, logger, filename = None):
		if logger is not None:
			self.logger = logger
		else:
			self.init = False
			return  # return the call since the logger has not been provided
		# Log that the file manager has been initialized
		self.logger.info("File Manager initialized")
		# Setting the filename
		if filename is not None:
			self.filename = filename
			self.logger.debug("Received filename : " + self.filename)
		else:
			self.filename = "./Default.tfile"  # TODO file extension
			self.logger.debug("Using default file : " + self.filename)

	def set_filename(self, filename = None):
		if filename is not None:
			self.filename = filename

	def read_file(self):
		line_list = []
		if self.filename is not None:
			# Read the file
			self.logger.info("Reading session file : " + self.filename)
			for line in FileInput([self.filename]):
				line_list.append(line.strip())

		self.logger.debug("Returning the line_list")
		return line_list

	def list_files(self):
		return glob(getcwd() + "/tfiles/*.tfile")

	def update_file(self, msg = []):
		# First check if the file already exists
		if self.check_file():
			self.logger.debug("File found to be existing : " + self.filename)
			# if the file exists  and the flag is None, change flag to append 
			# mode
			self._file_handler(msg, flag = 'w')
		else:
			self.logger.debug("File not found, creating : " + self.filename)
			self._file_handler(flag = 'w')

	def check_file(self):
		# Atomic function for just checking if the file exists or not
		if Path(self.filename).is_file():
			return True
		else:
			return False

	def _file_handler(self, msg = [], flag = None):
		# Default flag is write mode and not append mode
		if flag is None:
			flag = 'w'
		file_hnd = open(self.filename, flag)
		if len(msg) > 0:
			# Write in the file
			for i in range(len(msg)):
				file_hnd.writelines(str(msg[i]))
				file_hnd.write("\n")
		file_hnd.close()
